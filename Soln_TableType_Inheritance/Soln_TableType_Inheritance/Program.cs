﻿using Soln_TableType_Inheritance.Entity.Admin;
using Soln_TableType_Inheritance.Entity.user;
using Soln_TableType_Inheritance.Repository.admin;
using Soln_TableType_Inheritance.Repository.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {

                IEnumerable<userloginentity> listuserObj =
                   await new userrepository().Getuserdata;



                IEnumerable<adminloginentity> listadminobj =
                    await new adminrepository().Getadmindata;

            }).Wait();
        }
    }
}
