﻿using Soln_TableType_Inheritance.EF;
using Soln_TableType_Inheritance.Entity.Admin;
using Soln_TableType_Inheritance.Repository.person;
using Soln_TableType_Inheritance.Repository.user.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance.Repository.admin
{
   public class adminrepository :personrepository
    {
        //#region Declaration
        //private personEntities db = null;
        //#endregion

        //#region Constructor
        //public adminrepository()
        //{
        //    db = new personEntities();
        //}
        //#endregion

        #region Constructor
        public adminrepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<adminloginentity>> Getadmindata()
        {
            try
            {
                return await Task.Run(() => {

                    var getquery =
                      base.dbobject
                       //.db
                        .tbladmins
                        .OfType<tbladmin>()
                        .AsEnumerable()
                        .Select(this.selectadmindata)
                        .ToList();

                    return getquery;

                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tbladmin,adminloginentity> selectadmindata
        {
            get
            {
                return
                    (letbladminobj) => new adminloginentity()
                    {
                        password= letbladminobj.Apassword,
                        adminname = letbladminobj.Ausername
                        
                    };
            }
        }

        #endregion
    }
 }

