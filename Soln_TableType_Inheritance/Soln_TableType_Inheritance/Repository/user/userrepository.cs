﻿using Soln_TableType_Inheritance.EF;
using Soln_TableType_Inheritance.Entity.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance.Repository.user
{
    public class userrepository
    {
        //#region Declaration
        //private personEntities db = null;
        //#endregion

        //#region Constructor
        //public userrepository()
        //{
        //    db = new personEntities();
        //}
        //#endregion

        #region Constructor
        public userrepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<userloginentity>> Getuserdata()
        {
            try
            {
                return await Task.Run(() => {

                    var getquery =
                      base.dbobject
                     //.db
                        .tblusers
                        .OfType<tbluser>()
                        .AsEnumerable()
                        .Select(this.selectuserdata)
                        .ToList();

                    return getquery;

                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tbluser,userloginentity> selectuserdata
        {
            get
            {
                return
                    (letbluserobj) => new userloginentity()
                    {
                        username = letbluserobj.Uusername,
                        password = letbluserobj.Upassword
                        
                    };
            }
        }

        #endregion
    }
}
