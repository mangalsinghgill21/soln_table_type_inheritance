﻿using Soln_TableType_Inheritance.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance.Repository.person
{
    public class personrepository
    {
       
        #region Declaration
        private personEntities db = null;
        #endregion

        #region Constructor
        public personrepository()
        {
            db = new personEntities();

            this.dbobject = db;
        }
        #endregion

        #region Property
        public personEntities dbobject { get; set; }
        #endregion
    }
}
