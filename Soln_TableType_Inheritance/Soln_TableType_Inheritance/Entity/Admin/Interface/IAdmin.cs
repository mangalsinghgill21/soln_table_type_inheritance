﻿using Soln_TableType_Inheritance.Entity.Person.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance.Entity.Admin.Interface
{
    public interface IAdmin : Iperson
    {
        public string adminname { get; set; }
        public string password { get; set; }
    }
}
