﻿using Soln_TableType_Inheritance.Entity.Person.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_TableType_Inheritance.Entity.user.Interface
{
  public  interface Iuser : Iperson
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
